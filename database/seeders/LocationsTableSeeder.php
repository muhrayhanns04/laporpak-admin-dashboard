<?php

namespace Database\Seeders;

use App\Models\Province;
use Illuminate\Database\Seeder;
use Kavist\RajaOngkir\Facades\RajaOngkir;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listProvinces = RajaOngkir::provinsi()->all();
        foreach ($listProvinces as $province) {
            Province::create([
                'province_id' => $province['province_id'],
                'province' => $province['province']
            ]);
        }
    }
}
