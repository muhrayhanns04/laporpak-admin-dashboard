<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->longText('message');
            $table->string('time_of_occurrence')->nullable();
            $table->string('incident_date')->nullable();
            $table->string('incident_place')->nullable();
            $table->string('detail_place')->nullable();
            $table->enum('status', ['proses', 'selesai'])->default('proses');
            $table->enum('type_of_report', ['pengaduan', 'aspirasi',])->default('pengaduan');
            $table->enum('visibility', ['anonim', 'non-anonim',])->default('anonim');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
