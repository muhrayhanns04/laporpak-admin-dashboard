<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'url', 'report_id', 'reply_id', 'type', 'size'];

    public function report()
    {
        return $this->belongsTo(Report::class);
    }
    public function reply()
    {
        return $this->belongsTo(Reply::class);
    }
}
