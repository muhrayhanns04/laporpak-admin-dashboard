<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'time_of_occurrence' => $this->time_of_occurrence,
            'incident_date' => $this->incident_date,
            'incident_place' => $this->incident_place,
            'detail_place' => $this->detail_place,
            'type_of_report' => $this->type_of_report,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'user' => [
                'nik' => $this->user->nik,
                'name' => $this->user->name,
                'username' => $this->user->username,
                'email' => $this->user->email,
                'telp' => $this->user->telp,
                'level' => $this->user->level,
            ],
            'files' => FileResource::collection($this->files),
        ];
    }
}
