<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ReportResourceFull extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'time_of_occurrence' => $this->time_of_occurrence,
            'incident_date' => $this->incident_date,
            'incident_place' => $this->incident_place,
            'detail_place' => $this->detail_place,
            'type_of_report' => $this->type_of_report,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'user' => [
                'nik' => $this->user->nik,
                'name' => $this->user->name,
                'username' => $this->user->username,
                'email' => $this->user->email,
                'telp' => $this->user->telp,
                'level' => $this->user->level,
            ],
            'files' => FileResource::collection($this->files),
            'reply' => [
                'id' => $this->replies->id,
                'message' => $this->replies->message,
                'report_id' => $this->replies->report_id,
                'created_at' => Carbon::parse($this->replies->created_at)->format('Y-m-d'),
                'officer' => [
                    'name' => $this->replies->user->name,
                    'username' => $this->replies->user->username,
                    'email' => $this->replies->user->email,
                    'telp' => $this->replies->user->telp,
                    'level' => $this->replies->user->level,
                ],
                'files' => FileResource::collection($this->replies->files),
            ],
        ];
    }
}
