<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ResponseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'created_at' => $this->created_at,
            'officer' => [
                'name' => $this->user->name,
                'username' => $this->user->username,
                'telp' => $this->user->telp,
                'level' => $this->user->level,
            ],
            'files' => FileResource::collection($this->files),
            'report' => ReportResource::make($this->report),
        ];
    }
}
