<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'report_id' => $this->report_id,
            'reply_id' => $this->reply_id,
            // 'url' => "https://laporpakinc.solusi.vip" . Storage::url($this->url),
            'url' => "http://192.168.100.6:8000" . Storage::url($this->url),
            'created_at' => $this->created_at,
        ];
    }
}
