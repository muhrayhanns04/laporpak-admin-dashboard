<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Nik;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index(Request $request)
    {
        $results = $request->user();
        return response()->json(['message' => 'Berhasil mendapatkan user', 'results' => $results], Response::HTTP_OK);
    }
    public function login(Request $request)
    {
        $user = User::where("username", $request->username)->first();

        if (!$user) {
            return response()->json(['message' => 'Akun tidak ditemukan, silahkan register terlebih dahulu 😊'], Response::HTTP_EXPECTATION_FAILED);
        }

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json(['message' => 'Password tidak sesuai'], Response::HTTP_UNAUTHORIZED);
        }

        $token = $user->createToken('@laporpak')->plainTextToken;
        return response()->json(['message' => 'Berhasil login', 'token' => $token, 'token_type' => 'Bearer Token', 'results' => $user], Response::HTTP_OK);
    }

    public function register(Request $request)
    {
        $type = request('type');

        if ($type !== "villager") {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:35|min:4',
                'password' => 'required|string',
                'username' => "required|string|max:25|min:4",
                'telp' => "required|string|max:13",
                'level' => 'string'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:35|min:4',
                'password' => 'required|string',
                'nik' => 'required|string|max:16|min:16',
                'username' => "required|string|max:25|min:4",
                'telp' => "required|string|max:13",
                'level' => 'string'
            ]);
        }


        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $usernameFinded = User::where("username", $request->username)->first();

        if ($usernameFinded) {
            return response()->json(['message' =>  "Data sudah terdaftar, silahkan masuk"], Response::HTTP_FOUND);
        }

        $niks = Nik::where("nik", $request->nik)->first();
        $userNik = User::where("nik", $request->nik)->first();

        if ($request->level === "masyarakat") {
            if (!$niks) {
                return response()->json(['message' => 'Maaf NIK anda tidak ditemukan, silahkan coba lagi!'], Response::HTTP_UNAUTHORIZED);
            }
        }
        if ($userNik && $request->level === "masyarakat") {
            return response()->json(['message' => 'Maaf akun anda dengan NIK ini sudah terdaftar!'], Response::HTTP_UNAUTHORIZED);
        }

        $user = new User();
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->nik = $request->nik;
        $user->username = $request->username;
        $user->telp = $request->telp;
        $user->level = $request->level;
        $user->save();

        // $token = $user->createToken('@laporpak')->plainTextToken;

        return response()->json(['message' => 'Berhasil mendaftarkan sebagai ' . $user->level, 'results' => $user], Response::HTTP_CREATED);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->currentAccessToken()->delete();

        return response()->json([
            'message' => 'Berhasil keluar, sampai bertemu kembali',
        ],  Response::HTTP_OK);
    }
}
