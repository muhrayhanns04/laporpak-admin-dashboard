<?php

namespace App\Http\Controllers;

use App\Http\Resources\ResponseResource;
use App\Models\File;
use App\Models\Reply;
use App\Models\Report;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sort = request('sort');
        $value = request('value');
        $type = request('type');

        $replys = Reply::orderBy('created_at', 'desc')->get();

        if ($sort === "time" && $type === "free" && $value) {
            $complaint = Reply::whereDate("created_at", $value)->orderBy('created_at', 'DESC')->get();
            $replys = ResponseResource::collection($complaint);
        }

        return response()->json(['message' => 'Berhasil mendapatkan data response', 'results' => ResponseResource::collection($replys)], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $reply = new Reply;
        $reply->user_id = $request->user()->id;
        $reply->report_id = $request->report_id;
        $reply->message = $request->message;
        $reply->save();

        $files = $request->file("file");
        if ($request->hasFile("file")) {
            foreach ($files as $file) {
                $exploded = explode(".", $file->getClientOriginalName());
                $explodedResult = $exploded[count($exploded) - 1];
                $extension = $file->getClientOriginalExtension();
                $newName = rand(1, 9999) . '.' . $extension;

                Storage::putFileAs("public/replies", $file, $newName);

                File::create([
                    'name' => $newName,
                    'url' => 'public/replies/' . $newName,
                    'type' => $explodedResult,
                    'size' => $file->getSize(),
                    'reply_id' => $reply->id,
                ]);
            }
        }

        $report = Report::findOrFail($request->report_id);
        $report->status = "selesai";
        $report->update();

        return response()->json(['message' => 'Berhasil menanggapi laporan ', 'results' => ResponseResource::make($reply)], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request, $id)
    {
        $sort = request('sort');
        $value = request('value');

        if ($sort === "byToken") {
            $results = $request->user();
        }
        if ($sort === "byID") {
            $complaint = Reply::where('user_id', $id)->orderBy('created_at', 'desc')->get();
            $results = ResponseResource::collection($complaint);
        }

        if ($sort === "time" && $value) {
            $complaint = Reply::where('user_id', $id)->whereDate("created_at", $value)->orderBy('created_at', 'DESC')->get();
            $results = ResponseResource::collection($complaint);
        }

        return response()->json(['message' => 'Berhasil mendapatkan tanggapan', 'results' => $results], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reply = Reply::findOrFail($id);
        $reply->message = $request->message;
        $reply->update();

        return response()->json(['message' => 'Berhasil memperbaharui laporan ', 'results' => ResponseResource::make($reply)], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = Reply::findOrFail($id);

        $files = File::where('reply_id', $reply->id)->get();

        foreach ($files as $file) {
            Storage::delete($file->url);
            $file->delete();
        }

        $reply->delete();

        return response()->json(['message' => 'Berhasil menghapus tanggapan', 'results' => $reply], Response::HTTP_OK);
    }
}
