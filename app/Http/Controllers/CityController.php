<?php

namespace App\Http\Controllers;

use Kavist\RajaOngkir\Facades\RajaOngkir;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CityController extends Controller
{
    public function show($id)
    {
        $listProvinces = RajaOngkir::kota()->dariProvinsi($id)->get();
        return response()->json(['message' => 'Berhasil mendapatkan provinsi', 'results' => $listProvinces], Response::HTTP_CREATED);
    }
}
