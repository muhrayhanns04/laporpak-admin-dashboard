<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReportResource;
use App\Http\Resources\ReportResourceFull;
use App\Models\File;
use App\Models\Report;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaints = Report::orderBy('created_at', 'desc')->get();
        return response()->json(['message' => 'Berhasil mendapatkan data complaint', 'results' => ReportResource::collection($complaints)], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
            'time_of_occurrence' => 'required|string',
            'incident_date' => 'required|string',
            'incident_place' => 'required|string',
            'detail_place' => 'required|string',
            'type_of_report' => 'required|string',
        ]);

        if (Report::where("message", 'LIKE', '%' . $request->message . '%')->first()) {
            // dd(Report::where("message", $request->message)->get());
            return response()->json(['message' => 'Laporan serupa sudah ada, mohon maaf silahkan buat laporan yang berbeda', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $complaint = new Report;
        $complaint->user_id = $request->user()->id;
        $complaint->message = $request->message;
        $complaint->time_of_occurrence = $request->time_of_occurrence;
        $complaint->incident_date = $request->incident_date;
        $complaint->incident_place = $request->incident_place;
        $complaint->detail_place = $request->detail_place;
        $complaint->type_of_report = $request->type_of_report;
        $complaint->status = 'proses';
        $complaint->save();

        $files = $request->file("file");
        if ($request->hasFile("file")) {
            foreach ($files as $file) {
                $exploded = explode(".", $file->getClientOriginalName());
                $explodedResult = $exploded[count($exploded) - 1];
                $extension = $file->getClientOriginalExtension();
                $newName = rand(1, 9999) . '.' . $extension;

                Storage::putFileAs("public/reports", $file, $newName);

                File::create([
                    'name' => $newName,
                    'url' => 'public/reports/' . $newName,
                    'type' => $explodedResult,
                    'size' => $file->getSize(),
                    'report_id' => $complaint->id,
                ]);
            }
        }

        return response()->json(['message' => 'Berhasil membuat laporan ', 'results' => ReportResource::make($complaint)], Response::HTTP_CREATED);
    }

    public function filterFree()
    {
        $sort = request('sort');
        $value = request('value');
        $type = request('type');
        $value2 = request('second_value');

        if ($sort === "status" && $value && $type === "free") {
            $complaint = Report::where('status', $value)->orderBy('created_at', 'desc')->get();
            if ($value === "proses") {
                $results = ReportResource::collection($complaint);
            } else {
                $results = ReportResourceFull::collection($complaint);
            }
        }

        if ($sort === "byNIK" && $value) {
            $user = User::where("nik", $value)->first();

            if ($user) {
                $complaint = Report::where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
                $results = ReportResource::collection($complaint);
            } else {
                $results = [];
            }
        }

        if ($sort === "status_specific" && $value) {
            $complaint = Report::findOrFail($value)->where('status', 'selesai')->orderBy('created_at', 'desc')->first();
            $results = ReportResourceFull::make($complaint);
        }

        if ($sort === "status_with_time" && $value && $type === "free" && $value2) {
            $complaint = Report::where('status', $value)->whereDate("created_at", $value2)->orderBy('created_at', 'DESC')->get();
            if ($value === "proses") {
                $results = ReportResource::collection($complaint);
            } else {
                $results = ReportResourceFull::collection($complaint);
            }
        }

        return response()->json(['message' => 'Berhasil mendapatkan laporan', 'results' => $results], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $sort = request('sort');
        $value = request('value');
        $value2 = request('second_value');

        if ($sort === "byID" && $value) {
            $complaint = Report::where('id', $value)->first();
            $results = ReportResource::make($complaint);
        }
        if ($sort === "status" && $value) {
            $user = User::where("nik", $id)->first();
            $complaint = Report::where('user_id', $user->id)->where('status', $value)->orderBy('created_at', 'desc')->get();
            if ($value === "proses") {
                $results = ReportResource::collection($complaint);
            } else {
                $results = ReportResourceFull::collection($complaint);
            }
        }
        if ($sort === "byNIK" && $value) {
            $user = User::where("nik", $value)->first();
            $complaint = Report::where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
            $results = ReportResource::collection($complaint);
        }
        if ($sort === "byToken") {
            $results = $request->user();
        }
        if ($sort === "status_with_time" && $value && $value2) {
            $complaint = Report::where('status', $value)->whereDate("created_at", $value2)->orderBy('created_at', 'DESC')->get();
            if ($value === "proses") {
                $results = ReportResource::collection($complaint);
            } else {
                $results = ReportResourceFull::collection($complaint);
            }
        }

        return response()->json(['message' => 'Berhasil mendapatkan laporan', 'results' => $results], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $complaint = Report::findOrFail($id);
        $complaint->message = $request->message;
        $complaint->time_of_occurrence = $request->time_of_occurrence;
        $complaint->incident_date = $request->incident_date;
        $complaint->incident_place = $request->incident_place;
        $complaint->detail_place = $request->detail_place;
        $complaint->update();

        return response()->json(['message' => 'Berhasil memperbaharui laporan ', 'results' => ReportResource::make($complaint)], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $complaint = Report::findOrFail($id);

        $files = File::where('report_id', $complaint->id)->get();

        foreach ($files as $file) {
            Storage::delete($file->url);
            $file->delete();
        }

        $complaint->delete();

        return response()->json(['message' => 'Berhasil menghapus laporan', 'results' => $complaint], Response::HTTP_OK);
    }
}
