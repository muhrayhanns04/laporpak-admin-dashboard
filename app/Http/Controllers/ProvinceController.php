<?php

namespace App\Http\Controllers;

use Kavist\RajaOngkir\Facades\RajaOngkir;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProvinceController extends Controller
{
    public function index()
    {
        $listProvinces = RajaOngkir::provinsi()->all();
        return response()->json(['message' => 'Berhasil mendapatkan provinsi', 'results' => $listProvinces], Response::HTTP_CREATED);
    }
}
