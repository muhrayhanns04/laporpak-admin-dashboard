<?php

namespace App\Http\Controllers;

use App\Http\Resources\ResponseResource;
use App\Models\File;
use App\Models\Reponse;
use App\Models\Report;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responses = Reponse::all();
        return response()->json(['message' => 'Berhasil mendapatkan data response', 'results' => ResponseResource::collection($responses)], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'Input tidak valid', 'error' => $validator->errors()], Response::HTTP_UNAUTHORIZED);
        }

        $response = new Reponse;
        $response->user_id = $request->user()->id;
        $response->report_id = $request->report_id;
        $response->message = $request->message;
        $response->save();

        $files = $request->file("file");
        if ($request->hasFile("file")) {
            foreach ($files as $file) {
                $exploded = explode(".", $file->getClientOriginalName());
                $explodedResult = $exploded[count($exploded) - 1];

                Storage::putFileAs("public", $file, $file->getClientOriginalName());

                File::create([
                    'name' => $file->getClientOriginalName(),
                    'url' => 'public/responses' . $file->getClientOriginalName(),
                    'type' => $explodedResult,
                    'size' => $file->getSize(),
                    'response_id' => $response->id,
                ]);
            }
        }

        $report = Report::findOrFail($request->report_id);
        $report->status = "selesai";
        $report->update();

        return response()->json(['message' => 'Berhasil menanggapi laporan ', 'results' => ResponseResource::make($response)], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = Reponse::findOrFail($id);

        return response()->json(['message' => 'Berhasil mendapatkan tanggapan', 'results' => ResponseResource::make($response)], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Reponse::findOrFail($id);
        $response->message = $request->message;
        $response->update();

        return response()->json(['message' => 'Berhasil memperbaharui laporan ', 'results' => ResponseResource::make($response)], Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = Reponse::findOrFail($id);

        $files = File::where('response_id', $response->id)->get();

        foreach ($files as $file) {
            Storage::delete($file->url);
            $file->delete();
        }

        $response->delete();

        return response()->json(['message' => 'Berhasil menghapus tanggapan', 'results' => $response], Response::HTTP_OK);
    }
}
