<?php

namespace App\Http\Controllers;

use App\Http\Resources\FileResource;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::orderBy('created_at', 'desc')->get();
        return response()->json(['message' => 'Berhasil mendapatkan data semua files', 'results' => FileResource::collection($files)], Response::HTTP_OK);
    }

    public function sort()
    {
        $sort = request('sort');
        $value = request('value');

        if ($sort === "report_id" && $value) {
            $file = File::where('report_id', $value)->orderBy('created_at', 'desc')->get();
        }
        if ($sort === "reply_id" && $value) {
            $file = File::where('reply_id', $value)->orderBy('created_at', 'desc')->get();
        }

        return response()->json(['message' => 'Berhasil mendapatkan data file by laporan', 'results' => FileResource::collection($file)], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $folder = request('folder');
        $type = request('type');
        $value = request('value');
        $fileRequests = $request->file("file");

        if ($request->hasFile("file")) {
            foreach ($fileRequests as $file) {
                $exploded = explode(".", $file->getClientOriginalName());
                $explodedResult = $exploded[count($exploded) - 1];
                $extension = $file->getClientOriginalExtension();
                $newName = rand(1, 9999) . '.' . $extension;
                Storage::putFileAs("public/" . $folder, $file, $newName);

                if ($type === "report_id") {
                    File::create([
                        'name' => $newName,
                        'url' => 'public/' . $folder . '/' . $newName,
                        'type' => $explodedResult,
                        'size' => $file->getSize(),
                        'report_id' => $value,
                        'reply_id' => null,
                    ]);
                } else {
                    File::create([
                        'name' => $newName,
                        'url' => 'public/' . $folder . '/' . $newName,
                        'type' => $explodedResult,
                        'size' => $file->getSize(),
                        'reply_id' => $value,
                        'report_id' => null
                    ]);
                }
            }
        }

        return response()->json(['message' => 'Berhasil mengunggah file'], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $folder = request('type');
        $fileRequest = $request->file("file");

        $exploded = explode(".", $fileRequest->getClientOriginalName());
        $explodedResult = $exploded[count($exploded) - 1];

        Storage::putFileAs("public/" . $folder, $fileRequest, $fileRequest->getClientOriginalName());

        $file = File::findOrFail($id);
        Storage::delete($file->url);

        $file->name = $fileRequest->getClientOriginalName();
        $file->url = 'public/' . $folder . '/' . $fileRequest->getClientOriginalName();
        $file->type = $explodedResult;
        $file->size = $fileRequest->getSize();
        $file->update();

        return response()->json(['message' => 'Berhasil memperbaharui file', 'results' => FileResource::make($file)], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::findOrFail($id);
        Storage::delete($file->url);
        $file->delete();

        return response()->json(['message' => 'Berhasil menghapus file', 'results' => FileResource::make($file)], Response::HTTP_OK);
    }
}
