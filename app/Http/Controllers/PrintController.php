<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;


class PrintController extends Controller
{
    public function index($id)
    {
        $pdf = Report::findOrFail($id);
        $date = Carbon::parse($pdf->incident_date)->isoFormat('dddd, Do MMMM YYYY');
        return view('pdfb', compact("date", "pdf"));
    }

    public function download($id)
    {
        $pdf = Report::findOrFail($id);
        $date = Carbon::parse($pdf->created_at)->isoFormat('dddd, Do MMMM YYYY');
        $filedate =
            Carbon::parse($pdf->created_at)->isoFormat('d/M/Y');

        $file = PDF::loadView('pdf', compact('pdf', 'date'))->setOptions(['defaultFont' => 'Poppins', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $file->download($pdf->type_of_report . "-" . $pdf->user->name . "-" . $filedate . '.pdf');
    }
}
