<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Dokumen Laporpak</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }

    </style>
</head>

<body>
    <table width="100%">
        <tr>
            <td align="left">
                <h3>LAPOR<span style="color: green;">PAK</span></h3>
            </td>
            <td align="right">
                <h3 style="font-style: italic;color:red;">
                    {{ $pdf->status === 'proses' ? 'PROSES' : 'SELESAI' }}
                </h3>
            </td>
        </tr>
    </table>

    <div class="container-message d-flex flex-column align-items-stretch">
        <table width="100%">
            <tr>
                <td align="left">
                    <p>
                        Kepada Yth.<br />
                        Bapak/Ibu Petugas<br />
                        Di, {{ $pdf->incident_place }}
                    </p>
                </td>
                <td align="right">
                    {{ $date }}
                </td>
            </tr>
        </table>

        <p>
            Dengan hormat, kami ingin memberikan pengajuan <span
                style="font-style: italic;">{{ $pdf->type_of_report }}</span>,<br />
            Yang melaporkan di bawah ini:
        </p>

        <table width="70%">
            <tr>
                <td align="left">NIK</td>
                <td>: {{ $pdf->user->nik }}</td>
            </tr>
            <tr>
                <td align="left">Nama Lengkap</td>
                <td>: {{ $pdf->user->name }}</td>
            </tr>
            <tr>
                <td align="left">Nama Pengguna</td>
                <td>: {{ $pdf->user->username }}</td>
            </tr>
            <tr>
                <td align="left">Nomer Telepon</td>
                <td>: {{ $pdf->user->telp }}</td>
            </tr>
            <tr>
                <td align="left">Waktu Kejadian</td>
                <td>: {{ $pdf->time_of_occurrence }}</td>
            </tr>
            <tr>
                <td align="left">Tanggal Kejadian</td>
                <td>: {{ $date }}</td>
            </tr>
            <tr>
                <td align="left">Tempat Kejadian</td>
                <td>: {{ $pdf->incident_place }}</td>
            </tr>
            <tr>
                <td align="left">Detail Tempat</td>
                <td>: {{ $pdf->detail_place }}</td>
            </tr>
        </table>

        <p>
            {{ $pdf->message }}
        </p>
        <p>
            Mengingat penting nya laporan ini, kami berharap Bapak/Ibu untuk segera meresponse laporan ini.
            Demikian laporan yang kami sampaikan. Atas perhatian dan kerjasamanya kami ucapkan terimakasih.
        </p>

        <div class="ttd" style="position: absolute; bottom: 0;width:100%">
            <table width="100%">
                <tr style="text-align:left;">
                    <td align="left" style="width: 60%;">
                        Pelapor<br />
                        Jakarta<br /><br /><br />
                        ttd<br /><br /><br />
                        {{ $pdf->user->name }}
                    </td>
                    <td align="right" style="">
                        <p style="text-align: left;">
                            CEO LaporPak Indonesia<br />
                            Jakarta<br /><br /><br />
                            ttd<br /><br /><br />
                            Muhammad Rayhan
                        </p>
                    </td>
                </tr>
            </table>
        </div>


        {{-- <div class="footer" style="position: absolute; bottom: 0;width:100%">
            <table width="100%">
                <tr>
                    <td align="left">
                        <p>
                            &copy; {{ date('Y') }} LaporPak Indonesia - Seluruh hak cipta.
                        </p>
                    </td>
                    <td align="right">
                        Hans CEO of LaporPak
                    </td>
                </tr>
            </table>
        </div> --}}
    </div>
</body>

</html>
