<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('assets/css/app.css') }}>
    <style>
        .navbar .container {
            border: 1px solid blue;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
        }

    </style>
</head>

<body>
    <nav class="navbar navbar-expand-xl navbar-light bg-white py-3">
        <div class="container">
            <h3>Lapor<span class="text-success">Pak</span></h3>
            <h3 class="ml-auto text-success text-uppercase" style="font-style: italic;"> {{ $pdf->status }}</h3>
        </div>
    </nav>

    <div class="container d-flex flex-column align-items-stretch">
        <div class="d-flex justify-content-between">
            <p>
                Kepada Yth.<br />
                Bapak/Ibu {{ $pdf->user->name }}<br />
                Di, {{ $pdf->incident_place }}
            </p>
            <div class="date">{{ $date }}</div>
        </div>

        <p>
            Dengan hormat,<br />
            Yang melaporkan di bawah ini:
        </p>

        <table class="border-none mb-3">
            <tr>
                <th>NIK</th>
                <td>: {{ $pdf->user->nik }}</td>
            </tr>
            <tr>
                <th>Nama</th>
                <td>: {{ $pdf->user->username }}</td>
            </tr>
            <tr>
                <th>Nama Lengkap</th>
                <td>: {{ $pdf->user->name }}</td>
            </tr>
            <tr>
                <th>Nomor Telepon</th>
                <td>: {{ $pdf->user->telp }}</td>
            </tr>
        </table>

        <p>
            {{ $pdf->message }}
        </p>
        <p>
            Dengan ini,{{ request('name') }} melaporkan pengaduan yang terjadi di <span
                style="font-weight: 700;font-style: italic;">{{ $pdf->incident_place }}</span>
            dengan alamat lengkap <span style="font-weight: 700;font-style: italic;">{{ $pdf->detail_place }}</span>
        </p>

        <div class="d-flex justify-content-end" style="margin-top: 24px">
            <div class="text-center d-inline-block ml-auto">
                <img src="https://i.ibb.co/hYkrFBz/ttd.jpg" style="width: 50px" />
                <img src="{{ public_path('images/logo/logo.png') }}" style="width: 50px" />
                <p style="font-weight: 500; margin-top: 16px">Hans</p>
                <p style="font-weight: 800; margin-top: -16px">CEO LaporPak</p>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
