<?php

// use App\Http\Controllers\Api\AuthController;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\ReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Protected routes
Route::group(['middleware' => ['auth:sanctum', 'cors']], function () {
    Route::prefix('/complaints')->group(function () {
        Route::get('/', [ReportController::class, 'index']);
        Route::get('/filter/{id}', [ReportController::class, 'show']);
        Route::post('/create', [ReportController::class, 'store']);
        Route::patch('/update/{id}', [ReportController::class, 'update']);
        Route::post('/{id}', [ReportController::class, 'destroy']);
    });

    // Response
    Route::prefix('/responses')->group(function () {

        Route::get('/filter/{id}', [ReplyController::class, 'show']);
        Route::post('/create', [ReplyController::class, 'store']);
        Route::patch('/update/{id}', [ReplyController::class, 'update']);
        Route::post('/{id}', [ReplyController::class, 'destroy']);
    });

    Route::prefix('/files')->group(function () {
        Route::get('/', [FileController::class, 'index']);
        Route::get('/filter', [FileController::class, 'sort']);
        Route::post('/create', [FileController::class, 'store']);
        Route::post('/update/{id}', [FileController::class, 'update']);
        Route::post('/{id}', [FileController::class, 'destroy']);
    });

    Route::prefix('/profile')->group(function () {
        Route::get('/', [AuthController::class, 'index']);
    });

    Route::post('/logout', [AuthController::class, 'logout']);
});

Route::prefix('location')->group(function () {
    Route::get("/provinces", [ProvinceController::class, 'index']);
    Route::get("/provinces/{id}/cities", [CityController::class, 'show']);
});

Route::get('/responses/free', [ReplyController::class, 'index']);
Route::get('/complaints/free', [ReportController::class, 'filterFree']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/', function () {
    return response()->json(['message' => 'Documentaion API Laporpak'],);
});
